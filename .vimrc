call plug#begin()

let mapleader=" "

Plug 'junegunn/fzf', { 'dir': '~/.fzf', 'do': './install --all' }
Plug 'junegunn/fzf.vim'
Plug 'morhetz/gruvbox', { 'dir': '~/.vim/colors' }
Plug 'airblade/vim-gitgutter'
Plug 'sheerun/vim-polyglot'
Plug 'ctrlpvim/ctrlp.vim'
Plug 'scrooloose/nerdcommenter'
Plug 'tpope/vim-fugitive'
Plug 'itchyny/vim-gitbranch'
Plug 'jiangmiao/auto-pairs'
Plug 'simeji/winresizer'
Plug 'markonm/traces.vim'
Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'

set relativenumber
set termguicolors
set background=dark
set noshowmode
autocmd VimEnter * colorscheme gruvbox
autocmd VimEnter * AirlineTheme gruvbox
let g:airline_powerline_fonts = 1
""""Remappings""""
nnoremap <Leader>w <C-w>
call plug#end()

